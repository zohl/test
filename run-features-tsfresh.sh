#!/bin/bash

max_processes=8

list_fn=$1
force_flag=$2

tmux select-layout even-vertical

i=$(ps -aux | grep 'invest.features.tsfresh' | grep -v grep | sed 's/^.*python //' | sort | uniq | wc -l)

for d in $(echo 30 60 90 180 | tr ' ' '\n' | shuf); do
	[ "$i" -ge "$max_processes" ] && break

	for f in $(cat $list_fn | shuf); do
		[ "$i" -ge "$max_processes" ] && break

		if [ $(ps -aux | grep 'python -m invest.features.tsfresh' | grep "$f" | wc -l) -gt 0 ]; then
			echo "already processing $f"
			continue
		fi

		tmux split-window -v "python -m invest.features.tsfresh $f $force_flag -d $d"
		tmux select-layout
		((i++))

		# sleep 1
	done
done
echo "done"
