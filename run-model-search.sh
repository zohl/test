#!/bin/bash

max_processes=12


tmux select-layout even-vertical

i=$(ps -aux | grep 'invest.models search' | grep -v grep | sed 's/^.*python //' | sort | uniq | wc -l)

for f in $(find ./artifacts/models/ -maxdepth 1 -iname '*.yaml' | shuf); do
	[ "$i" -ge "$max_processes" ] && break

    echo ">>> $f"
	[ -f "${f%.yaml}/report.yaml" ] && continue

    if [ $(ps -aux | grep 'python -m invest.models search' | grep "$f" | wc -l) -gt 0 ]; then
    	echo "already processing $f"
    	continue
    fi

 	tmux split-window -v "python -m invest.models search $f -n 32"
 	tmux select-layout
 	((i++))

 	sleep 1

done

echo "done"
