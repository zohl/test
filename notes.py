def partial_corrs(xs0, xss, alpha):
    [l0, r0] = list(xs0.astype(float).quantile([alpha/2, 1 - alpha/2]))
    idx0 = (xs0 >= l0) & (xs0 <= r0)

    for xs1 in xss:
        [l1, r1] = list(xs1.astype(float).quantile([alpha/2, 1 - alpha/2]))
        idx = idx0 & (xs1 >= l1) & (xs1 <= r1)

        loss = 1 - idx.mean()
        if loss > 1 - alpha:
            logging.warning(f'significant loss of data on {xs0.name} x {xs1.name} ({loss:0.2f})')
            yield np.nan
        else:
            yield xs0[idx].corr(xs1[idx])


def filter_by_corrs(df, threshold, alpha = 0.05, verbose = False):
    cs = list(df.columns)
    result = []

    pbar = tqdm(total = len(cs)) if verbose else None

    while len(cs) > 0:
        [c1, *cs] = cs
        result.append(c1)

        n_left = len(cs)
        cs = _c(list, _map(fst),
                reversed, _sort(abs, snd),
                _filter(lambda x: np.isnan(x) or abs(x) < threshold, snd),
                _l(zip, cs), partial_corrs)(df[c1], [df[c] for c in cs], alpha)

        if pbar is not None:
            pbar.update(n_left - len(cs) + 1)

    return result
